/**
 * Esta classe � utilizada para testar erros e aprendizagem de try...catch.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.erros;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DividePorZero {

	
	public static void dividir(Scanner teclado) throws InputMismatchException, ArithmeticException {
		
		System.out.println("N�mero: ");
		int n = teclado.nextInt();
		
		System.out.println("Divisor: ");
		int divisor = teclado.nextInt();
		
		System.out.println("Resultado: " + n / divisor);
		
	}
	
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		
		boolean continua = true;
		
		do {
			
			try {
				//Chama o m�todo divis�o
				dividir(teclado);
				
				//Se a divis�o for feita corretamente, para de executar
				continua = false;
			}catch(ArithmeticException | InputMismatchException e) {
					
				System.err.println("Valor inv�lido!!!");
				
				//Descarta  a entrada do usu�rio
				teclado.nextLine();
				
			} finally {
				
				System.out.println("Finally Executado!");
				
			} // Fim do finally
			
		}while(continua);
		
	} // Fim do bloco principal

} // Fim da classe
