/**
 * Interface de fun��o calcular volume para corpos.
 */

package br.senai.sp.informatica.heranca;

public interface VolumeCalculavel {

	public double CalculaVolume();
	
}
