/**
 * Classe para instancias um quadrado.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public class Quadrado implements AreaCalculavel{
	
	//Atributo
	double lado;

	//Construtor
	public Quadrado(double lado) {
		super();
		this.lado = lado;
	} // Fim do construtor

	@Override
	public double calculaArea() {
		return Math.pow(lado, 2);
	} // Fim do m�todo calculaArea
	
} // Fim da classe
