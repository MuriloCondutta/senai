/**
 * Esta classe tem a fun��o de inst�nciar objetos cubo.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public class Cubo implements AreaCalculavel, VolumeCalculavel {

		double lado;
		
		
	public Cubo(double lado) {
			super();
			this.lado = lado;
		}


	@Override
	public double calculaArea() {
		return 6 * Math.pow(lado, 2);
	}


	@Override
	public double CalculaVolume() {
		return Math.pow(lado, 3);
	}
	
}
