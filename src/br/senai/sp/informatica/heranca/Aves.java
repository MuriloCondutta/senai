/**
 * Esta classe tem a fun��o de fornecer especifica��es para animais aves.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public abstract class Aves extends Animal{
	
	public Aves(double peso, String comida) {
		super(peso, comida);
	}

	void voar() {
		
		System.out.println("Estou voando... J� voou!");
		
	}
	
	void botar() {
		
		System.out.println("Estou botando... J� botou um ovo");
		
	}
	
}
