/**
 * Esta classe tem a fun��o de ser pai dos objetos animais mais espec�ficos, como aves e cachorros.
 * Possui m�todos gerais para todos os animais.
 * 
 * @author Murilo Afonso Condutta
 */


package br.senai.sp.informatica.heranca;

//Declara��o da classe Animal como abstrata - que n�o possui uma defini��o �nica.
public abstract class Animal {

	//Atributos
	protected double peso;
	protected String comida;
	protected String nome;
	
	//Construtor
	public Animal(double peso, String comida) {
		this.peso = peso;
		this.comida = comida;
	}
	
	
	//Caso na classe pai tenha dois construtores, pode-se chamar apenas um nas classes filhas.
	public Animal(double peso, String comida, String nome) {
		this.peso = peso;
		this.comida = comida;
		this.nome = nome;
	}



	//M�todos
	final void dormir() {
		
		System.out.println("O animal dormiu!");
		
	}
	
	//M�todo abstract n�o possui corpo, est� para ser declarado nas classes filhas
	abstract void fazerBarulho();
	
}
