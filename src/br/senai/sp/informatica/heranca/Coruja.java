/**
 * Esta classe tem a fun��o de inst�nciar corujas.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public final class Coruja extends Aves {

	public Coruja(double peso, String comida) {
		super(peso, comida);
		
	}

	@Override
	void fazerBarulho() {
		
		System.out.println("Pru! Pru!");
		
	}

}
