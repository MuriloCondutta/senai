/**
 * Classe para o in�cio do estudo de interfaces(polimorfismo)
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public interface AreaCalculavel {

	double calculaArea();
	
} // Fim a interface
