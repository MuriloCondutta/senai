/**
 * Esta classe possui a operação multiplicação.
 * 
 * @author Murilo Afonso Condutta
 */


package br.senai.sp.informatica.heranca.Matematica;

public class Multiplicacao extends OperacaoMatematica {

	double calcular(double x, double y) {
		return x*y;
	}
	
}
