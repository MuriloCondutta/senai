/**
 * Esta classe possui a opera��o soma.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca.Matematica;

public class Soma extends OperacaoMatematica {

	@Override
	double calcular(double x, double y) {
		return x+y;
	}
	
}
