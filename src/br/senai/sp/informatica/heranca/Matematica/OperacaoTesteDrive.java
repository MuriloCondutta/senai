/**
 * Esta classe tem a fun��o de testar os m�todos nas classes do pacote matem�tica.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca.Matematica;

public class OperacaoTesteDrive {

	public static void calc(OperacaoMatematica o, double x, double y) {

		System.out.println(o.calcular(x, y));
		
		
	} // Fim do m�todo calc

	public static void main(String[] args) {

		Soma soma = new Soma();
		double r = soma.calcular(5, 6); // 11
		System.out.println(r);
		
		Soma somaDois = new Soma();
		calc(somaDois , 5, 10); // 15
		
		Multiplicacao multiplicacao = new Multiplicacao();
		
		calc(multiplicacao, 5, 10); // 50
		
		calc(new Multiplicacao(), 50, 2); // 100
		

	} // Fim do m�todo principal
} // Fim da classe
