/**
 * Esta classe tem a fun��o de inst�nciar cachorros.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public final class Cachorro extends Animal{
	
	public Cachorro(double peso, String comida) {
		super(peso, comida);
	}

	void enterrarOsso() {
		
		System.out.println("Enterrou um osso!");
		
	}
	
	//Polimorfismo - sobrescrita de m�todo
	void fazerBarulho() {
		System.out.println("Au! Au! Au!");
	}
}
