/**
 * Esta classe tem a fun��o de testar o que foi feito nas classes interligadas.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public class AnimalTesteDrive {
	
	//Implementa��o do polimorfismo de forma �gil
	public static void barulho(Animal a) {
		
		a.fazerBarulho();
		
	} // Fim do m�todo barulho

	public static void main(String[] args) {
		
		 Animal toto = new Cachorro(10, "carne");
		
		System.out.println("-----------Cachorro tot�-----------");
	//	toto.enterrarOsso();
		toto.fazerBarulho();
		toto.dormir();
				
		Galinha carijo = new Galinha(5, "Milho");
		
		System.out.println("-----------Galinha Carij�-----------");
		carijo.botar();
		carijo.voar();
		carijo.fazerBarulho();
		carijo.dormir();

		Coruja bonitinha = new Coruja(3, "Rato");
			
		System.out.println("-----------Coruja Bonitinha-----------");
		bonitinha.botar();
		bonitinha.voar();
		bonitinha.fazerBarulho();
		bonitinha.dormir();
			
		//Teste de tipos
		System.out.println("-----------Teste de Tipos-----------");
		System.out.println(toto instanceof Cachorro); //true
		System.out.println(toto instanceof Animal); //true
		System.out.println(toto instanceof Galinha); //false, pois toto � um animal, n�o uma galinha
		
		/*
		 *  Com o abstract na classe Animal, n�o � poss�vel inst�nciar algum objeto do tipo Animal. Apenas de classes concretas.
		 *  Animal generico= new Animal(0, null);
		 *  
		 *  System.out.println("-----------Animal gen�rico-----------");
		 *  barulho(generico);
		 *  
		 *  Ave ave = new Ave(20, "Milho");
		*/
		
		// Testes do m�todo barulho(polimorfismo)
		System.out.println("-----------Teste de barulho(polimorfismo)-----------");
		barulho(toto);
		barulho(carijo);
		barulho(bonitinha);
		
		
	} // Fim do m�todo main

}