package br.senai.sp.informatica.heranca;

public final class Passaro extends Animal{

	public Passaro(double peso, String comida) {
		super(peso, comida);
	}

	@Override
	void fazerBarulho() {
		
		System.out.println("Piu! Piu!");
		
	}

}
