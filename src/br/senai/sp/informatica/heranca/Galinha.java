/**
 * Esta classe tem a fun��o de inst�nciar galinhas.
 * 
 * @author Murilo Afonso Condutta
 */


package br.senai.sp.informatica.heranca;

public final class Galinha extends Aves implements AreaCalculavel{

	public Galinha(double peso, String comida) {
		super(peso, comida);

	}
	
	void fazerBarulho() {
		System.out.println("Co C� Ri C�");
	}

	//Em jogos, deve-se calcular a �rea de uma galinha a tela por conta das colis�es, da� se adiciona a interface AreaCalcul�vel
	@Override
	public double calculaArea() {
		return 0;
	}

}
