/**
 * Esta classe testa as classes que envolvem o aprendizado de Interfaces: AreaCalculavel, VolumeCalculavel, Quadrado, Cubo.
 * 
 * @author Murilo Afonso Condutta
 */

package br.senai.sp.informatica.heranca;

public class InterfaceTesteDrive {
	
	public static void area(AreaCalculavel a) {
		
		System.out.println(a.calculaArea());
		
	}
	
	public static void volume(VolumeCalculavel a) {
		
		System.out.println(a.CalculaVolume());
		
	}

	public static void main(String[] args) {

		Quadrado quadrado = new Quadrado(5);
		System.out.println("Area: " + quadrado.calculaArea());
		
		Cubo cubo = new Cubo(5);
		System.out.println("Cubo:");
		System.out.println("Area: " + cubo.calculaArea());
		System.out.println("Volume: " + cubo.CalculaVolume());
		
		//Utilizando o m�todo area
		System.out.println("-----------------Utilizando o m�todo geral: -----------------");
		area(new Quadrado(2));
		area(new Cubo(2));
		
		volume(new Cubo(2));
		
		// volume(new Quadrado(2)); <-- Possui erro pois Quadrado n�o possui volume

	} // Fim do m�todo principal

} // Fim da classe
