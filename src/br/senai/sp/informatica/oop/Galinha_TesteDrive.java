package br.senai.sp.informatica.oop;

import java.util.ArrayList;

public class Galinha_TesteDrive {

	public static void main(String[] args) {
		
		ArrayList<Galinha> lista = new ArrayList<Galinha>();

		Galinha coco = new Galinha();

		coco.botar().botar().botar();
		System.out.println(coco.ovos);

		lista.add(coco);
		
		Galinha penosa = new Galinha();
		
		lista.add(penosa);
		
		penosa.botar().botar();
		System.out.println(penosa.ovos);
		System.out.println(Galinha.totalOvosGranja);
		
		System.out.println(Galinha.MediaOvos(lista.size()));
	}

}
