package br.senai.sp.informatica.oop;

/**
 * Esta classe foi utilizada para aprendizado sobre objetos. 
 * 
 * @author Murilo Afonso Condutta
 *
 */

public class Cachorro {

	
	//Atributos
	String raca;
	int tamanho;
	public String nome;  //Est� p�blico para ser usado fora do pacote
	
	//M�todos
	void latir() { // M�todo para o cachorro latir
		
		System.out.println("O " + this.nome + " latiu: \"Au Au Au !!!\"");
		
	} // Fim do m�todo latir
	
} // Fim da classe cachorro
