package br.senai.sp.informatica.oop;

public enum Medida {
	MM("Milímetro"), CM("Centímetro"), M("Metro");
	
	//Atributos
	public String titulo;
	
	//Construtor
	Medida(String titulo) {
		
		this.titulo = titulo;
		
	}
}
