package br.senai.sp.informatica.oop;

/**
 * Esta classe � criada para metodificar opera��es matem�ticas.
 * 
 * Classe com opera��es matem�ticas:
 * 
 * <ol>
 * 
 * <li>Soma</li>
 * <li>Raiz</li>
 * <li>Maior</li>
 * 
 * </ol>
 * 
 * @author Murilo Afonso Condutta
 */

public class Matematica {

	// Atributos

	// ------------

	// M�todos

	// MAIOR
	/**
	 * Este m�todo mostra o maior entre dois n�meros.
	 * 
	 * @param um
	 *            primeiro para comparar.
	 * @param dois
	 *            segundo numero para comparar.
	 * @return maior numero entre os dois.
	 */
	int maior(int um, int dois) {

		if (um > dois) { // Verifica se o primeiro � maior que o segundo
			return um; // Caso sim, reotrne o primeiro
		} else {
			return dois; // Caso contr�rio, retorne o segundo
		}

	} // Fim do m�todo maior

	// SOMA
	double soma(double um, double dois) {

		return um + dois;

	} // Fim do m�todo soma

	// SUBTRA��O
	int subtracao(int um, int dois) {

		return um - dois;

	} // Fim do m�todo subtracao

	// RAIZ
	int raiz(int numero) {

		// Equa��o de Pell
		int passos = 0;

		for (int i = 1; i <= numero; i += 2) {

			++passos;
			numero -= i;
		}

		return passos;

	} // Fim do m�todo raiz

	// SOMA COM CONJUNTOS DE N�MEROS
	/**
	 * Soma um conjunto de n�meros.
	 * 
	 * @param numeros
	 *            - conjunto de n�meros para somar.
	 * @return soma dos n�meros do conjunto.
	 */
	double soma(double... numeros) {

		double total = 0;

		// Percorre o vetor de n�meros, somando cada n�mero encontrado (foreach);
		for (double numero : numeros) {
			total += numero;
		}
		return total;
	} // Fim do m�todo soma de conjuntos de numeros

	// M�DIA
	/**
	 * Este m�todo realiza o c�lculo de m�dia aritm�tica de dois n�meros.
	 * 
	 * @param x
	 *            - primeiro n�mero
	 * @param y
	 *            - segundo n�mero
	 * @return Retorna a soma dos dois n�meros, dividido por dois.
	 */
	double media(int x, int y) {
		System.out.println("media(int x, int y)");
		return (x + y) / 2;

	} // Fim do m�todo media

	// M�DIA POR STRING
	/**
	 * Este m�todo realiza a m�dia aritm�tica de dois n�meros vindos como String.
	 * Utiliza a convers�o com classes Wrapper. Pode ser utilizado com JOptionPane.
	 * 
	 * @param x
	 *            - primeira String.
	 * @param y
	 *            - Segunda String.
	 * @return - Retorna a soma dos dois n�meros, dividido por dois.
	 */
	double media(String x, String y) {

		System.out.println("media(String x, String y)");

		// Converte de String para int
		int ix = Integer.parseInt(x);
		int iy = Integer.parseInt(y);

		// C�lcula a m�dia e retorna a m�dia
		return (ix + iy) / 2;
		//return media(ix, iy);
	}

} // Fim da classe