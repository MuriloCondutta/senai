package br.senai.sp.informatica.oop;

public class Carro_TesteDrive {

	public static void main(String[] args) {

		// Exemplo construtor vazio
		Carro ferrari = new Carro();
		ferrari.modelo = "Ferrari Enzo";
		ferrari.segundosZeroACem = 3.2;
		ferrari.velocidadeMaxima = 349;
		
		Motor v12 = new Motor();
		v12.tipo = "v12";
		v12.potencia = 660;
		
		//Associa o motor com o carro ferrari
		
		ferrari.motor = v12;

		// Exemplo construtor com par�metros - obriga-se fornecer as informa��es dos
		// par�metros (menor risco de esquecimento)
		Carro koenigsegg = new Carro("Kenigsegg CCXR", 430, 2.3);
		
		Motor v8 = new Motor("v8", 1018);
		koenigsegg.motor = v8;
		
		Carro bugatti = new Carro("Bugatti Veron", 430, 2.2, new Motor("W16", 1200));
		
		//Imprimir dados sobre o carro
		System.out.println("Dados do Carro: ");
		System.out.println("Modelo: " + bugatti.modelo);
		System.out.println("Velocidade M�xima: " + bugatti.velocidadeMaxima + " Km/h");
		System.out.println("Segundos de 0 a 100Km/h: " + bugatti.segundosZeroACem);
		System.out.println("Tipo do motor: " + bugatti.motor.tipo);
		System.out.println("Pot�ncia do motor: " + bugatti.motor.potencia + " cavalos");
	}

}
