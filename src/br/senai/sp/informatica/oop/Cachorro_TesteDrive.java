package br.senai.sp.informatica.oop;

/**
 * Este algortimo � utilizado para testar a classe Cachorro.
 * 
 * @author Tecnico_Manha
 *
 */

public class Cachorro_TesteDrive {

	public static void main(String[] args) {
		
		//Make a instance
		Cachorro pitbull = new Cachorro();
		pitbull.raca = "Pitbull";
		pitbull.tamanho = 45;
		pitbull.nome = "Sadam";
		
		pitbull.latir();
		
		Cachorro toto = new Cachorro();
		toto.raca = "vira-lata";
		toto.tamanho = 32;
		toto.nome = "Tot�";
		
		toto.latir();
	} // End of the method main
	
	//This faz refer�ncia ao objeto

} // Class end
