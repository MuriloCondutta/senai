package br.senai.sp.informatica.oop;

/**
 * Esta classe � criada para metodificar opera��es banc�rias.
 * 
 * @author Murilo Afonso Condutta
 */

public class Conta {

	// Account attributes
	String cliente;
	double saldo;

	void exibeSaldo() {

		System.out.println("Cliente: " + cliente);
		System.out.println("Saldo: R$" + saldo);

	} // Fim do m�todo exibeSaldo

	void sacar(double valor) {

		saldo -= valor;

	} // Fim do m�todo sacar

	void depositar(double valor) {

		saldo += valor;

	} // Fim do m�todo depositar

	void transferePara(Conta destino, double valor) {

		this.sacar(valor);
		destino.depositar(valor);

	} // Fim do m�todo transferePara

} // Fim da classe
