package br.senai.sp.informatica.oop;

/**
 * Este algoritmo realiza o teste da classe Pessoa.
 * 
 * @author Murilo Afonso Condutta
 *
 */
public class Pessoa_TesteDrive {

	public static void main(String[] args) {
		
		Pessoa Caroline = new Pessoa();
		
		Caroline.nome = "Caroline";
		Caroline.idade = 35;
		
		System.out.println(Caroline.idade); //Imprime a idade atual
		
		Caroline.aniversario(); //Adiciona um ano � idade = 42
		Caroline.aniversario(); //Adiciona um ano � idade = 43
		Caroline.aniversario(); //Adiciona um ano � idade = 44
		Caroline.aniversario(); //Adiciona um ano � idade = 45
		
		System.out.println(Caroline.idade); //Imprime a idade final
		
		Pessoa Mateus = new Pessoa();
		
		Mateus.nome = "Mateus";
		Mateus.idade = 41;
		
		System.out.println(Mateus.idade); //Imprime a idade atual
		
		Mateus.aniversario(); //Adiciona um ano � idade = 33
		Mateus.aniversario(); //Adiciona um ano � idade = 34
		Mateus.aniversario(); //Adiciona um ano � idade = 35
		
		System.out.println(Mateus.idade); //Imprime a idade final
		
		System.out.println(Caroline.idade - Mateus.idade); //Imprime a idade atual
		
	} // fim do m�todo principal
	
} // Fim da classe
