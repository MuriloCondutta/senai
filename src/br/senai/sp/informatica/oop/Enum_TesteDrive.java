package br.senai.sp.informatica.oop;

public class Enum_TesteDrive {

	public enum Sexo {
		M("MASCULINO"), F("FEMININO");
		String s;

		Sexo(String s) {
			this.s = s;
		}
	}

	// Constante
	public static final double PI = 3.14;

	public static void main(String[] args) {

		System.out.println(PecasXadrez.BISPO);
		System.out.println(Medida.CM);
		System.out.println(Medida.CM.titulo);
		System.out.println(Sexo.M.s);

		for (Medida m : Medida.values()) {
			System.out.println(m + ":" + m.titulo);
		}

	}
}
