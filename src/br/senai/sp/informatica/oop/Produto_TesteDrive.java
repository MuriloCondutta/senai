package br.senai.sp.informatica.oop;

public class Produto_TesteDrive {
	public static void main(String[] args) {
		
		Produto shampoo = new Produto();

		System.out.println("-------------------------------------------------");
		shampoo.setCodigoDeBarras("6H54255145");
		System.out.println("Codigo de Barras: " + shampoo.getCodigoDeBarras());

		shampoo.setDescricao("anti-caspa");
		System.out.println("Descri��o do Produto: " + shampoo.getDescricao());

		shampoo.setCategoria("beleza");
		System.out.println("Categoria do Produto: " + shampoo.getCategoria());

		shampoo.setPreco(15);
		System.out.println("Preco do Produto: " + shampoo.getPreco());

		shampoo.setPerecivel(false);
		System.out.println("Perecivel? " + shampoo.isPerecivel());
		System.out.println("-------------------------------------------------\n");

		Produto iogurte = new Produto();

		System.out.println("-------------------------------------------------");
		iogurte.setCodigoDeBarras("FDSE482F45F5");
		System.out.println("Codigo de Barras: " + iogurte.getCodigoDeBarras());

		iogurte.setDescricao("iogurte de morango");
		System.out.println("Descri��o do Produto: " + iogurte.getDescricao());

		iogurte.setCategoria("gelada");
		System.out.println("Categoria do Produto: " + iogurte.getCategoria());

		iogurte.setPreco(4);
		System.out.println("Preco do Produto: " + iogurte.getPreco());

		iogurte.setPerecivel(true);
		System.out.println("Perecivel? " + iogurte.isPerecivel());
		System.out.println("-------------------------------------------------\n");
		
		float soma = shampoo.getPreco() + iogurte.getPreco();
		System.out.println("Valor total da compra: " + soma);
		
	}
}
