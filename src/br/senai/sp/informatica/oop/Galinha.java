package br.senai.sp.informatica.oop;

public class Galinha {
	
	public static int totalOvosGranja; //Vari�vel global para contabilizar ovos da granja inteira

	int ovos; // Ovos que a Galinha botou
	
	public Galinha botar() {
		ovos++;
		totalOvosGranja++;
		return this;
	}
	
	public static double MediaOvos(double galinhas) {
		return Galinha.totalOvosGranja / galinhas;
	}
	
}
