package br.senai.sp.informatica.oop;

public class Produto {

	private String codigoDeBarras;
	private String descricao;
	private float preco;
	private String categoria;
	private boolean perecivel;

	public String getCodigoDeBarras() {
		return codigoDeBarras;
	}

	public void setCodigoDeBarras(String codigoDeBarras) {
		if (!codigoDeBarras.isEmpty() && codigoDeBarras.length() >= 10 && codigoDeBarras != null) {
			this.codigoDeBarras = codigoDeBarras;
		} else {
			System.out.println("C�digo inv�lido, digite novamente!");
		}
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		if (!descricao.isEmpty() && descricao.length() >= 5 && descricao != null) {
			this.descricao = descricao;
		} else {
			System.out.println("Descri��o inv�lida, digite novamente!");
		}
	}

	public float getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		if (preco > 0) {
			this.preco = preco;
		} else {
			this.preco = 1;
			System.out.println("Pre�o inv�lido, digite novamente!");
		}
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		if (!categoria.isEmpty() && categoria.length() >= 5 && categoria != null) {
			this.categoria = categoria;
		} else {
			System.out.println("Categoria inv�lida, digite novamente!");
		}
	}

	public boolean isPerecivel() {
		return perecivel;
	}

	public void setPerecivel(boolean perecivel) {
		this.perecivel = perecivel;
	}

}
