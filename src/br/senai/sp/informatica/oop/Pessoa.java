package br.senai.sp.informatica.oop;
/**
 * Classe criada para criar objetos pessoas.
 * 
 * M�todo:
 * 
 * <ol>
 * <li>aniversario</li>
 * </ol>
 * 
 * @author Murilo Afonso Condutta
 *
 */
public class Pessoa {

	public String nome; //Est� p�blico para ser usado fora do pacote
	int idade;
	
	//M�todos
	
	void aniversario(){
		
		this.idade++; //Adiciona um ano � idade do objeto referido
		System.out.println("O(a) " + this.nome + " fez anivers�rio!");
		
	} //Fim do m�todo anivers�rio
} //Fim da classe
