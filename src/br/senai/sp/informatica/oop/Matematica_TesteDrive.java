package br.senai.sp.informatica.oop;

/**
 * Este algoritmo servir� como teste da classe Matematica.
 * 
 * @Author Murilo Afonso Condutta
 */

public class Matematica_TesteDrive {

	public static void main(String[] args) {

		// Cria uma inst�ncia na classe matem�tica

		Matematica m = new Matematica();

		System.out.println("Maior: " + m.maior(32, 2));

		int adicao = (int) m.soma(32, 2); // Realiza o cast expl�cito do resultado do m�todo, que est� para retornar
											// double.

		System.out.println("Soma: " + adicao);
		
		// ------------ Desafio ------------
		
		System.out.println("Soma maior - uma linha: " + m.maior(2, 4) + m.maior(3, 5)); // <-- m�todo eficiente
		
			//Outro m�todo
			int par = m.maior(2, 4);
			int impar = m.maior(3, 5);
			double soma = par + impar;
			System.out.println("Soma maior: " + soma);

			//Raiz
			System.out.println("Raiz: " + m.raiz(81));
			
			//Square Root = Raiz Quadrada
			double raiz = Math.sqrt(276);
			System.out.println("Raiz pelo Square Root: " + raiz);
			
			//Teste de soma de conjuntos de n�meros
			System.out.println("Soma de conjuntos de n�meros: " + m.soma(10, 56, 89, 101));
			
			//Teste de m�todo de m�dia
			System.out.println(m.media(10, 20));
			
	} // Fim do m�todo principal

} // Fim da classe
