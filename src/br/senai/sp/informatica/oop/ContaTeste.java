package br.senai.sp.informatica.oop;

/**
 * Este algotimo servir� de teste da classe Conta.
 * 
 * @author Murilo Afonso Condutta
 */

public class ContaTeste {

	public static void main(String[] args) {

		Conta conta = new Conta();

		conta.cliente = "Murilo";
		conta.saldo = 10_000.00;

		Conta destino = new Conta();
		
		destino.cliente = "Bob";
		destino.saldo = 100.00;
		
		// Transfere o dinheiro de uma conta � outra
		conta.transferePara(destino, 2600.00);
		
		conta.exibeSaldo();
		destino.exibeSaldo();
		
		/*
		conta.exibeSaldo();
		conta.sacar(1000);
		conta.exibeSaldo();
		conta.depositar(1000);
		conta.exibeSaldo();
		*/
		
	}

}
