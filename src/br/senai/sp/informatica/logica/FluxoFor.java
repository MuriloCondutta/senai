package br.senai.sp.informatica.logica;
/**

	Programa para testar lações/fluxos de repetição
	
	!Curiosidade: podemos utilizar o i em todos os fluxos, pois cada i (iterador) pertence ao escopo do "for",
	não pertencendo a todo o programa.

*/

public class FluxoFor{
	
	public static void main(String[] args){

		//for
		for(int i = 0; i< 3; i++){

			System.out.println(i);

		} // Fim do fluxo de repetição que incrementa

		for(int i = 10; i >= 0; i--){

			System.out.println(i);

		} // Fim do fluxo de repetição que decrementa

		// Exibe todos os numeros pares entre 0 e 20
		for(int i = 0; i <= 20; i++){
			
			// Verifica se o número é par
			if(i % 2 == 0){
				
				System.out.println(i);
			
			} // Fim do fluxo condicional de módulo
			
		} // Fim do fluxo de repetição que imprime números pares entre 0 e 20
		
		for(int castigo = 0; castigo <= 5; castigo++){
			
				System.out.println("Não devo atirar papel nos colegas");
			
		} // Fim do fluxo d erepetição para escrever frase sde castigo na tela
	
	
	/*	
		for(int x = 0; true; x++){ // Enquanto for verdadeiro sempre rodará, mais se ja é verdadeiro, ficará eternamente contando, até que alguém pressione "Ctrl C"
	
				System.out.println(x);
		
		} // Fim do fluxo de repetição para imprimir eternamente na tela
	*/
	

		//Imprime um quadrado na tela
		int tamanho = 70;
		
		for(int j = 0; j < tamanho; j++){ // Fluxo de repetição para alterar a linha
		
			for(int i  = 0; i < tamanho; i++){ // Fluxo de repetição para imprimir 5 vezes o asterisco
				
				System.out.print("*"); //Imprime 1 asterisco, por vez que rodar o laço
			
			} // Fim do laço de repetição para imprimir 5 vezes o asterisco
			
			System.out.println(); // Pula uma linha
			
			
		} // Fim do fluxo de repetição para alterar linha
		
		//
		
	
	} // Fim do métood principal

} // Fim da classe