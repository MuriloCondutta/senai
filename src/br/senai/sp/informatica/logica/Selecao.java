package br.senai.sp.informatica.logica;
public class Selecao{
	public static void main(String [] args){
		
		char sexo = 'M';
		
		switch(sexo) { //Estrutura de seleção
			
			case 'M':
			case 'm':
			
				System.out.println("Masculino");
			break;
			
			case'F':
			case 'f':
			
				System.out.println("Feminino");
			break;
			
			default:
			
				System.out.println("Outro");
			
		} //Fim do switch
		
		String tecnologia = "java";

		switch(tecnologia) {
			
			case "java":
			case "c++":
			case "cobol":
			
				System.out.println("Linguagem de programa\u00e7\u00e3o");
			break;
				
			case "oracle":
			case"mysql":
			case "postgree":
			
				System.out.println("Banco de dados");	
			break;
			
			default: 
			
				System.out.println("Tecnologia desconhecida");
		}// Fim do switch tecnologia
		
	} //Fim do método principal

}  //Fim da classe