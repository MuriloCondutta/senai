package br.senai.sp.informatica.logica;
//import java.javax.swing.JOptionPane;

public class OperadorEspecial {
	public static void main(String[] args){
		
		//Operador ternário - trabalha com três operandos
		int idade = 36;
		
		String x = idade >= 18 ? "Maior de Idade" : "Menor de Idade";
		
		System.out.println(x);
		
		boolean y = idade >= 18 ? true : false;
		
		System.out.println(y);
		
		//Operador Separador de Expressões:
		
		String sexo = "M", pais = "Brasil";
		System.out.println(sexo);
		System.out.println(pais);
	}
}