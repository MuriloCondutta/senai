package br.senai.sp.informatica.logica;
public class OperadorAtrib {
	public static void main(String[] args){
	//Operadores de Atribuição
	
	int x = 6;
	
	//Soma +3 na variavel x
	//x += 3;
	System.out.println(x);
	
	//Subtração
	//x = x-3;;
	x -=3 ;
	System.out.println(x);
	
	//Multiplicação
	// x= x*3;
	x *= 3;
	System.out.println(x);
	
	//Divisão
	//x = x/3;
	x /= 3;
	System.out.println(x);
	
	//Módulo - resto da divisão
	//x = x % 3;
	x %= 3;
	System.out.println(x);
	
	}
}