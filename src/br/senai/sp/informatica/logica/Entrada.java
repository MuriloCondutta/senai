package br.senai.sp.informatica.logica;
//import java.util.Scanner;
import javax.swing.JOptionPane;

public class Entrada{
	public static void main(String[] args){ //Os colchetes significam vetor (conjunto de dados)
		
		//Mostra um diálogo para o usuário e guarda o nome do usuário na variável nome
		String nome = JOptionPane.showInputDialog("Qual e o seu nome?");
		
		//Mostra uma mensagem de boas-vindas para o usuário
		JOptionPane.showMessageDialog(null, "Bem-vindo " + nome + "!!!");
	
		//Cria uma entrada de dados		
		//Scanner teclado = new Scanner(System.in);
		
		//Interagindo com o usuário
		//System.out.println("qual o seu nome?");
		
		//Leia(nome)
		//faz a leitura do que o usuário digitar e guarda na variável nome
		
		//String nome = teclado.nextLine();
		
		//System.out.println("Bem-Vindo " + nome);
		
		//Imprime o primeiro argumento (parâmetro) do método main
		// System.out.printl(args[0] + " ");
		// System.out.println(args[1]);
	
	}
}