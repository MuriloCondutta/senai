package br.senai.sp.informatica.logica;

public class AutoBoxing{
	public static void main(String[] args){
		//Boxing = empacotamento
		Integer x = new Integer(555);
		x++;
		System.out.println(x);
		
		if(x == 556) {
			
			System.out.println("V");
		
		} else {
			
			System.out.println("F");
		
		}	
			
		}	
		//----------Até versão 5 do JAVA --------------//
		/*
		
		//---INT---//
		//Recupera o valor de x
		//unboxing - desempacotar
		
		int a = x.intValue();
		
		//x = 555 + 1 incrementar
		a++;
		
		//Re-empacotar
		x = new Integer(a);
		
		//---BOOLEAN---//
		Boolean v = new Boolean("true");
		//Entrair para um tipo primitivo
		if(v.booleanValue()){
			
			System.out.println("V");
		
		} else {
			
			System.out.println("F");
		
		}	
		*/
	}
