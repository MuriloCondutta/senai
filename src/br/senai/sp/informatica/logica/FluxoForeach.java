package br.senai.sp.informatica.logica;
import java.util.ArrayList;

public class FluxoForeach{
	public static void main(String[] args) {
	
		int[] pares = {2, 4, 6, 8};
		
		//Foreach
		for(int par : pares) {
			
				System.out.println(par);
			
		}	
		
		//Foreach com String
		String[] nomes = { "Jorge", "Julia", "Ana", "Pedro"};
		
		//Foreach
		for(String nome : nomes){
			
				System.out.println(nome);
			
		}	
	
		//Foreach com ArrayList
		ArrayList<Integer> lista = new ArrayList<Integer>();
		
		//Adiciona 10 elementos no Array List
		for(int i = 0; i <= 10; i++){
			
				lista.add(i);
			
		}	
		
		//Imprime todo o ArrayList
		for(int valor : lista){
			System.out.println(valor);
		}
	}
}