package br.senai.sp.informatica.logica;
public class ArrayMulti {
	public static void main(String[] args){
	
	//VETORES:
	
		//Declarando um array para pessoas:
		String[] vetor = { //Declaração de vetor - dois colchetes.
	
		"Ricardo",
		"Sandra",
		"Beatriz"
		};
	
		System.out.println(vetor[0]);
	
	
		System.out.println(vetor.length); //imprime o tamanho do vetor (quantos indices possui)
		
	// MATRIZES:
	
		String[][] matriz = { //Declaração de matriz - quatro colchetes.
		
			{"Ricardo", "M", "DF"},
			{"Sandra", "F", "MG"},
			{"Beatriz", "F", "DF"}					
	
		}; //Fim da declaração da matriz
		
		//Imprimindo ficha do Ricardo
			System.out.print(matriz[0][0] + ","); // Sempre: matriz[Linha][Coluna] 
			System.out.print(matriz[0][1] + ",");
			System.out.println(matriz[0][2]);
		
		//Imprime ficha da Beatriz
			System.out.print(matriz[2][0] + ","); 
			System.out.print(matriz[2][1] + ",");
			System.out.println(matriz[2][2]);
			
		//Imprime ficha da Sandra
			System.out.print(matriz[1][0] + ","); 
			System.out.print(matriz[1][1] + ",");
			System.out.println(matriz[1][2]);
		
		//Tamanho da matriz
			System.out.print("Tamanho da matriz: ");
			System.out.println(matriz.length); //Tamanho da matriz inteira - número de linhas
			System.out.println(matriz[0].length); //Tamanho da linha da matriz
			System.out.println(matriz[1].length); //Tamanho da linha da matriz
			
		//Total de elementos na matriz
			//int total = matriz.length * matriz[0].length - PRIMEIRA FORMA DE SABER QUANTOS ELEMENTOS TEM NA MATRIZ
		
		
		
	}//Fim do main
} //Fim da classe