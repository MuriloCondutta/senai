package br.senai.sp.informatica.logica;
public class Operador {
	public static void main(String[] args){
		
		//Operadores de comparação
			int x = 6;
			String o = "6";
			System.out.println(x == 6); //true
			System.out.println(o == "6"); //true
			System.out.println(x != 6); //false
			System.out.println(x != 7); //true
			System.out.println(x > 7); //false
			System.out.println(x > 5); //true
			System.out.println(x >= 5); //true
			System.out.println(x >= 6); //true
		
			//instanceof = compara tipos <-- Só funciona com classes, sme tipode de dados primitivos
			Integer l = 6;
			//Compara se l é do tipo Integer
			System.out.println(l instanceof Integer);
			//System.out.println("oi" instanceof String);
		
		//Operadores lógicos
		
			//operador && (e)
			Integer meuInt = 6;
			System.out.println((meuInt >= 1) && (meuInt <= 10)); //true
			System.out.println((meuInt >= 1) && (meuInt <= 1)); //false
			
			//operador || (ou)
			Integer meuInt2 = 7;
			
		//Operadores matemáticos
		
			//double x = 7 + 3; // 10
			//double x = 7 * 3; // 21
			//double x = 3 % 2; // 1
		
			//int x = 6;
			//int y = --x; //y = x - 1
			//System.out.println(y);
			
			//operador binário
			//int x = 9 + 4;		
			//System.out.println(x);
		
			//precedência - multiplicação e divisão primeiro, adição e subtração depois: 
			//todas na ordem que aparecerem (da esuqerda para a direita) 
		
			//double y = 5 + 3 / 2; // 6
			//double y = (5 + 3) / 2; // 4
			//double y = 7 - 4 + 3 * 2; // 9
			//double y = (7 - 4 + 3) * 2; // 12
			//System.out.println(y);
	
	}
}