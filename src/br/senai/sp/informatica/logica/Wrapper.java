package br.senai.sp.informatica.logica;
public class Wrapper {
	public static void main(String[] args){
	
	//Cria uma variável do tipo double
	
	Double preco = new Double("12.45");
	System.out.println(preco);
	
	//Converte Double para double
	double valor = preco.doubleValue();
	System.out.println(valor);
	
	//Converte Double para int
	int valorInt = preco.intValue();
	System.out.println(valorInt);
	
	//Converte Double para byte
	byte valorByte = preco.byteValue();
	System.out.println(valorByte);
	
	Boolean casado = new Boolean("false");
	
	//Conersão Estática
	String valorTxt  ="123.45";
	
	//Converte String para Double
	double myDouble = Double.parseDouble(valorTxt);
	System.out.println(myDouble);
	
	//Converte de String para int
	String intTxt = "123";
	int myInt = Integer.parseInt(intTxt);
	System.out.println(myInt);

	//Converte de String para float
	String floatTxt = "3.14F";
	float myFloat = Float.parseFloat(floatTxt);
	
	int binario = Integer.valueOf("101011");
	System.out.println(binario);
	
	int decimal = Integer.valueOf("101011", 2);
	System.out.println(decimal);
	
	}
}
