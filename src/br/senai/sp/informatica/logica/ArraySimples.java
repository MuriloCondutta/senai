package br.senai.sp.informatica.logica;
import java.util.Arrays;

public class ArraySimples{
	public static void main(String[] args){
		
		//Cria um array de Strings:
			String[] paises = {"Brasil", "R\u00fassia", "Canad\u00e1", "India", "China"};
		
		//Imprime os dados dos elementos:
		
			//O primeiro elemento:
				System.out.println(paises[0]);
		
			//O quarto elemento:
				System.out.println(paises[3]);
				
			//Mostra todos os elementos:
			System.out.println(Arrays.toString(paises));
				
		//Quantos elementos tem no array:
		
			System.out.println(paises.length);
			
/*---------------------------------------------------------------------------------------------------------------*/			
		//Pesquisa se um elemento existe no array:
		
			int indice = Arrays.binarySearch(paises, "Canad\u00e1");
			System.out.println(indice);
			System.out.println(paises[indice]);
		
/*---------------------------------------------------------------------------------------------------------------*/		
		//Ordenacao:
		
		  //Parametros:(array, indice_inicio, indice_final)
			Arrays.sort(paises, 0, paises.length); //<-- no indice final, coloca-se todos os elementos, para que a ordenação seja até o final.
			System.out.println(Arrays.toString(paises));
			
			Double[] valores = {12.35, 3456.3456, 19.12};
			System.out.println(valores[0]);
		
		/* Declara um array int para 5 elementos */
			int[] impares = new int[5]; // Criando um array sem declarar valores, por isso colocamos o numero de posicoes
			
			//Inicializa os elementos do array:
			impares[0] = 1;
			impares[1] = 3;
			impares[2] = 5;
			impares[3] = 7;
			impares[4] = 9;
			
			System.out.println(impares[1]);
			

		}
}