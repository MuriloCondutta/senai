package br.senai.sp.informatica.logica;
/**
*O programa escreve uma mensagem na tela
*@Author Murilo Afonso Condutta
*/

public class HelloWorld {	//Início da classe HelloWorld

	// Função Principal
	
	public static void main(String[] args) {	//Início do método main
		
	/*
	*Escreve Hello World! na tela
	*/
	
	System.out.println("\"\tHello\nJava!\"");
	
	}//Fim do método main
}// Fim da classe