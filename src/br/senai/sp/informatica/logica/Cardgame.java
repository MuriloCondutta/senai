package br.senai.sp.informatica.logica;
import java.util.Random;
import javax.swing.JOptionPane;

public class Cardgame {
	public static void main(String[] args) {
		
		//Declaração de vetores:
			String[] faces = {"A", "2", "3", "4", "5", "6", "7", "8", "9", 
			"10", "J", "Q", "K"};
		
			String[] naipes = {"Espadas", "Paus", "Copas", "Ouros"};
		
		//Declaração de variáveis para sorteio:
		
			Random random = new Random();
			random.nextInt();
			
		//Sorteia um índice no vetor de faces:
		
			int indiceFaces = random.nextInt(faces.length);
			
		//Sorteia uma face:
		
			String face = faces[indiceFaces];
			
		//Sorteia um índice no vetor de naipes:
		
			int indiceNaipes = random.nextInt(naipes.length);
			
		//Sorteia um naipe:
		
			String naipe = naipes[indiceNaipes];
		
		//Seleciona uma carta:
		
			String cartas = face + " " + naipe;
		//	System.out.println(cartas);
		
		JOptionPane.showMessageDialog(null, cartas);
	}
}