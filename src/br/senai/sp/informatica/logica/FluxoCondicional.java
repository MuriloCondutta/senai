package br.senai.sp.informatica.logica;
public class FluxoCondicional{
	public static void main(String [] args){
		
		int idade = 10;
		
		if(idade <11) {
			
			System.out.println("Crian\u00e7a");
			
		}
		
		boolean passou = true;
		
		if(passou == true) {
			
			System.out.println("Contratado");
			
		}
		
		if(!passou){
			
		System.out.println("N\u00e3o contratado");
		
		}
		
		//Verifica se o número é par:
		
		int numero = 200;
		
		if(numero %2 == 0){
			
			System.out.println("O n\u00famero \u00e9 par!");
			
		} else {
			
			System.out.println("O n\u00famero \u00e9 impar!");
			
		}
		
	}//Fim do main
}//Fim da classe