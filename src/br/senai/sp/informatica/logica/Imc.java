package br.senai.sp.informatica.logica;
/*!
	Imc Prototipo (Imc)
	
	Este programa realiza o calculo de IMC para o Hackanton da Saude e o exibe na tela, 
	com informacoes de classificacao. 
	
	@Author: Murilo Afonso Condutta @SENAI de Informatica
*/
//Classes importadas:
import java.util.Scanner;

//Programa:
public class Imc{
	public static void main(String[] args){
		
		//Boas-Vindas: cumprimenta e recebe o nome
			System.out.println("Olá usuário, precisamos de algumas informacoes!");
			System.out.println("Qual o nome do paciente?");
			Scanner nome = new Scanner(System.in);
			String name = nome.nextLine();
		
		//Calculo de imc
		//IMC = PESO / ATURA ^ 2
		
		//Declaracao de peso, altura e do resultado do calculo:
			double pesoEmKg = 75, alturaEmMetros = 1.76, imc = 0;
		
		//CALCULO DO IMC:
			imc = pesoEmKg / Math.pow(alturaEmMetros, 2);
			
		//Classificacao do paciente:
			String classificacao = imc >= 20 && imc <= 25 ? "SHOW DE BOLA!!! Paciente dentro do peso ideal" : "POXA!!! Paciente fora do peso ideal";
		
		
		//Exibe o resultado:
			System.out.println("----------------------------------");
			System.out.println("Paciente: " + name);
			System.out.println("IMC: " + imc);
			
			
			System.out.println("Diagnostico: " + classificacao);
	}
}