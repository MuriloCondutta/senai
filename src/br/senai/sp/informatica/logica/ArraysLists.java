package br.senai.sp.informatica.logica;
import java.util.ArrayList;

public class ArraysLists{
	public static void main (String[] args){
	
		//Declara ArrayList:
	
			ArrayList<String> cores = new ArrayList<String>();
	
		//Adiciona itens ao ArrayList:
		
			cores.add("Branco");
			cores.add("Azul");
			cores.add("Amarelo");
			cores.add(0, "Vermelho");
		
		//Exibe todos os itens do ArrayList
			System.out.println(cores.toString());
		
		//Mostra quantos elemntos há no array:
		
			System.out.println(cores.size());
		
		//Seleciona um item do ArrayList
		
			System.out.println(cores.get(2));
		
		//Remove um item do arrayList:
		
			cores.remove("Branco");
			System.out.println(cores.toString());
		
		//Verifica se existe um determinado valor no vetor, retornando true or false:
		
			System.out.println(cores.contains("Azul"));
			System.out.println(cores.contains("Verde"));
		
		//Verifica se não tem vermelho:
			cores.contains("Vermelho"); 
			System.out.println(!cores.contains("Vermelho"));
		
	}	
}