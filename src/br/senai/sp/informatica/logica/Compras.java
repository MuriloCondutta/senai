package br.senai.sp.informatica.logica;
import java.util.ArrayList;
import java.util.Scanner;

public class Compras {
	public static void main(String[] args) {
		
		ArrayList<String> produtos = new ArrayList<String>();
		
		Scanner teclado = new Scanner(System.in);
		
		String mensagem = "Liste seus produtos. Para sair digite FIM";
		System.out.println(mensagem);
		
		String produto;
		//Enquanto "FIM" NÃO FOR IGUAL ao que o usuário digitar, continue executando.
		while(!"FIM".equals( produto = teclado.nextLine())){ //Sempre utilizar String.equals() para comparar Strings <-- Observe que String não é um dado primitivo, é uma classe Wrapper
															// Para negar a expressão e comparar se é diferente, utiliza-se a exclamação no começo da expres~sao, como na declaração do laço de repetição acima.
			// Adiciona produtos ao ArrayList produtos
			produtos.add(produto);
			
		}
		
		//Exibe os produtos
		System.out.println(produtos.toString());
		
		//Foreach
		
		for(String p : produtos) {
		
			System.out.println(p);
			
		}
	}
}