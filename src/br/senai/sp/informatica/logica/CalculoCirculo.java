package br.senai.sp.informatica.logica;
import java.util.Scanner;

public class CalculoCirculo {
	public static void main(String [] args){
	
	Scanner teclado = new Scanner(System.in);
	System.out.println("Qual o valor do raio?");
	
	//Recebe o valor do raio, do tipo double
	double raio = teclado.nextDouble();
	
	//Apresenta o valor do raio:
	System.out.println("Raio: " + raio);
	
	//Diâmetro = 2r
	double diametro = 2 * raio;
	System.out.println("Diametro: " + diametro);
	
	//Circunferência = 2PIr
	//double PI = Math.PI;
	//System.out.println("PI: " + PI);
	double circ = 2 * Math.PI * raio;
	System.out.println("Circunferencia: " + circ);
	
	//Area de um circulo:
	//A = PI * r ^ 2
	//double area = Math.PI * (raio * raio);
	double area = Math.PI * Math.pow(raio, 2);
	System.out.println("Area: " + area);
	}
}