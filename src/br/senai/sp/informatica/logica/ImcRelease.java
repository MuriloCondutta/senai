package br.senai.sp.informatica.logica;
/*!
	Imc Release v.1.0 (Imc)
	
	 - Este programa realiza o calculo de IMC e o exibe na tela um diagnostico 
	informando se o paciente esta com peso ideal ou fora do peso. 
	
	 - O peso ideal é caracterizqado por um imc entre 20 e 25.
	
	 - Formula para calculo de IMC:
	
			imc = peso / altura ^2
	
	@Author: Murilo Afonso Condutta @SENAI de Informatica
*/
//Classes importadas:
import javax.swing.JOptionPane;

//Programa:
public class ImcRelease {
	
	public static void main(String[] args){
	
	//Boas-Vindas: cumprimenta e recebe o nome
		JOptionPane.showMessageDialog(null, "Ola usuario, precisamos de algumas informacoes!", "Bem-Vindo", JOptionPane.PLAIN_MESSAGE);
		
	//Recebe o nome:		
		String nome = JOptionPane.showInputDialog(null, "Qual o nome do paciente?", "Nome", JOptionPane.QUESTION_MESSAGE);
		
	//Recebe o peso:
		String peso = JOptionPane.showInputDialog(null, "Qual seu peso em Kilogramas?", "Peso", JOptionPane.QUESTION_MESSAGE);

	//Converte peso em String para Double - conversao estatica:
		double pesoEmKg = Double.parseDouble(peso);
		
	//Recebe a altura:
		String h = JOptionPane.showInputDialog(null, "Qual sua altura em metros?", "Altura", JOptionPane.QUESTION_MESSAGE);
		
	//Converte altura em String para Double - conversao estatica:
		double alturaEmMetros = Double.parseDouble(h);
		
	//Calcula o IMC:
		double imc = pesoEmKg / Math.pow(alturaEmMetros, 2);
		
	//Classifica o paciente:
		String classificacao = imc >= 20 && imc <= 25 ? "SHOW DE BOLA!!! Dentro do peso ideal!" : "POXA!!! Fora do peso ideal";
	
	//Verifica a dica a ser dada ao paciente:
		String dica = imc >= 20 && imc <= 25 ? "Continue assim!" : "Esta na hora de fazer alguma dieta e sair do celular!";
	
	//Exibe o diagnostico:
		String diagnostico = "Paciente: " + nome + "\n" + "IMC: " + imc + "\n" + "Classificacao: " + classificacao + "\n" + "Dica: " + dica;
		JOptionPane.showMessageDialog(null, diagnostico, "Diagnostico", JOptionPane.PLAIN_MESSAGE);
	}
}