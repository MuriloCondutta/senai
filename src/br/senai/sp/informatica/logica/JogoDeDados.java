package br.senai.sp.informatica.logica;
/* 

	Programa que realiza o sorteio de dois numeros nos dados, os soma e compara com o palpite antes pedido ao usuário.

	Caso o palpite esteja correto, ou seja, de acordo com o reusltado, o usuário acerta; caso contrário, erra. Nos dois casos é mostrado
	uma mensgem de resultado, exibindo o nome do usuário, palpite e resultado da soma dos numeros sorteados, bem como uma mensagem esclarecendo se acertou ou não.

	@Author: Murilo Afonso Condutta @ SENAI de Informática

	*/

//Importa classes:
import javax.swing.JOptionPane;
import java.util.Random;
 
//Programa: 
public class JogoDeDados{
	public static void main(String [] args){
		
		//Declaração dos vetores para os dados
		
		String[] dadoUm = {"1", "2", "3", "4", "5", "6"};
		String[] dadoDois = {"1", "2", "3", "4", "5", "6"};
		
		//Declaração da variável que receberá valor aleatório
		
			//Dado 1
				Random dado1 = new Random();
				dado1.nextInt();
		
			//Dado 2
				Random dado2 = new Random();
				dado2.nextInt();

		//Modo Trapaça
			boolean modoTrapaca = false;

		//Aleatoriza os valores dos dados

				int numeroUm = dado1.nextInt(dadoUm.length);
				int numeroDois = dado2.nextInt(dadoDois.length);		

			//Soma os valores obtidos e guarda o resultado
				int resultado = numeroUm + numeroDois;

		
		//Mensagem de boas vindas
			JOptionPane.showMessageDialog(null, "Bem-vindo ao jogo de dados!", "Bem-Vindo", JOptionPane.PLAIN_MESSAGE);

		if(modoTrapaca){

			System.out.println(resultado);

		}
		
		//O jogo:
		
			String nomeUsuario = JOptionPane.showInputDialog(null, "Qual o seu nome?", "Nome", JOptionPane.QUESTION_MESSAGE);
			
			String palpite = JOptionPane.showInputDialog(null, "Ser\u00e1 sorteado um n\u00famero, em dois dados, que v\u00e3o de 2 ate 12. Nos de um palpite:", "Palpite", JOptionPane.QUESTION_MESSAGE);

		//Recolhimento e processamento de dados:

			//Coonverte o palpite em número inteiro
				int palpiteConvertido = Integer.parseInt(palpite);

			if((palpiteConvertido < 1) || (palpiteConvertido > 6)){

				String palpiteRefaccao = JOptionPane.showInputDialog(null, "Escolha um nu\u00famero entre 0 e 6:", "Alerta", JOptionPane.WARNING_MESSAGE);

				int palpiteConvertidoRefaccao = Integer.parseInt(palpite);

				String mostraResultado = nomeUsuario + ", " + "aqui est\u00e1 o resultado: " + "\n" + "Palpite: " + palpiteConvertidoRefaccao + "\n" + "Resultado dos dados: " + resultado + "\n" ;


				if(palpiteConvertido == resultado) { //Compara resultado e palpite, já mostrando o resultado do jogo na tela

					JOptionPane.showMessageDialog(null, mostraResultado + "Parab\u00e9ns, voc\u00ea acertou o n\u00famero sorteado!", "Resultado", JOptionPane.INFORMATION_MESSAGE);

				} else {

					JOptionPane.showMessageDialog(null, mostraResultado + "Poxa, da pr\u00f3xima voc\u00ea acerta!", "Resultado", JOptionPane.ERROR_MESSAGE);

				} //Fim da condicional de comparação de resultado com palpite
			} else {

			//JOptionPane.showMessageDialog(null, palpiteConvertido); // <-- Descomente está linha para mostrar a conversão do palpite

				String mostraResultado = nomeUsuario + ", " + "aqui est\u00e1 o resultado: " + "\n" + "Palpite: " + palpiteConvertido + "\n" + "Resultado dos dados: " + resultado + "\n" ;


				if(palpiteConvertido == resultado) { //Compara resultado e palpite, já mostrando o resultado do jogo na tela

					JOptionPane.showMessageDialog(null, mostraResultado + "Parab\u00e9ns, voc\u00ea acertou o n\u00famero sorteado!", "Resultado", JOptionPane.INFORMATION_MESSAGE);

				} else {

					JOptionPane.showMessageDialog(null, mostraResultado + "Poxa, da pr\u00f3xima voc\u00ea acerta!", "Resultado", JOptionPane.ERROR_MESSAGE);

				} //Fim da condicional de comparação de resultado com palpite

			} // Fim do else	
		
	} // Fim do método principal
} // Fim da classe