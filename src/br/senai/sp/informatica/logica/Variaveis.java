package br.senai.sp.informatica.logica;
	public class Variaveis {
			public static void main(String[] args) {
				
				//Declaração de Variáveis
				
				String nome = "Murilo";
				int idade = 16;
				boolean casado = false;
				double preco = 12.45;
				char sexo = 'M'; //unicode
				
				
				//Tipos inteiros:
				
				byte b = 127; // <--- o número máximo que este tipo de dado guarda (0-127)
				short s = 32767; // <--- o número máximo que este tipo de dado guarda (0-32767)
				int i = 2_000_000_000; // <--- o número máximo que este tipo de dado guarda (0-2 bilhões)
				long l = 9_000_000_000_000_000_000L; // <--- o número máximo que este tipo de dado guarda (0-9...)
				
				
				//Ponto Flutuante (númertos com casas decimais):
				
				double d = 1.7976931348623157E+308; // IEEE754 <--- norma do númeor máximo alcançado pelo tipo de dado double
				float f = 123.85F; //Números menores (com menos casas decimais)
				
				//1 byte = 8bits
				byte bb = 0b01010101; //44
				
				//2 bytes = 16 bits
				short ss = 0b0101010101010101;
				
				//4 bytes = 32 bits;
				int ii = 0b01010101010101010101010101010101;
				
				//Cast Implícito
				int meuInt = 32;
				short meuShort=80;
				meuInt = meuShort;
				System.out.println(meuInt); //803
				
				//Cast Explícito
				int meuInt2 = 32;
				long meuLong = 2_000_000;
				meuInt2 = (int)meuLong;
				System.out.println(meuInt2);
				
				//Impressão na tela:
				System.out.println("Nome: " + nome);
				System.out.println("Idade: " + idade);
				System.out.println("Casado(a): " +casado);
			}	//Fim do método main
	}	//Fim da classe variaveis